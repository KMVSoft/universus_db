CREATE TABLE `Student` (
	`ID` bigint NOT NULL AUTO_INCREMENT,
	`Name` varchar(50) NOT NULL,
	`Surname` varchar(50) NOT NULL,
	`Patronymic` varchar(50) NOT NULL,
	`GroupID` bigint NOT NULL,
	`BirthDay` DATE NOT NULL,
	`emailAddress` varchar(50) NOT NULL,
	PRIMARY KEY (`ID`)
);

CREATE TABLE `StudentGroup` (
	`ID` bigint NOT NULL AUTO_INCREMENT,
	`StudentGroupName` varchar(25) NOT NULL,
	`CreatedDate` DATE NOT NULL,
	`TutorID` bigint NOT NULL,
	`GroupLeaderID` bigint NOT NULL,
	`DepartmentID` bigint NOT NULL,
	`SpecialityID` bigint NOT NULL,
	PRIMARY KEY (`ID`)
);

CREATE TABLE `Teacher` (
	`ID` bigint NOT NULL AUTO_INCREMENT,
	`Name` varchar(50) NOT NULL,
	`Surname` varchar(50) NOT NULL,
	`Patronymic` varchar(50) NOT NULL,
	`emailAddress` varchar(50) NOT NULL,
	PRIMARY KEY (`ID`)
);

CREATE TABLE `University` (
	`ID` bigint NOT NULL AUTO_INCREMENT,
	`Name` varchar(255) NOT NULL UNIQUE,
	`Address` varchar(255) NOT NULL,
	`Web_Address` varchar(255) NOT NULL,
	PRIMARY KEY (`ID`)
);

CREATE TABLE `Exam` (
	`ID` bigint NOT NULL AUTO_INCREMENT,
	`Title` varchar(50) NOT NULL,
	`Description` mediumtext NOT NULL,
	`FileURL` varchar(50) NOT NULL,
	PRIMARY KEY (`ID`)
);

CREATE TABLE `ExamHistory` (
	`ID` bigint NOT NULL AUTO_INCREMENT,
	`StudentID` bigint NOT NULL,
	`TestID` bigint NOT NULL,
	`DateOfExam` DATETIME NOT NULL,
	PRIMARY KEY (`ID`)
);

CREATE TABLE `Department` (
	`ID` bigint NOT NULL AUTO_INCREMENT,
	`Name` varchar(255) NOT NULL,
	`UniversityID` bigint NOT NULL,
	PRIMARY KEY (`ID`)
);

CREATE TABLE `Speciality` (
	`ID` bigint NOT NULL AUTO_INCREMENT,
	`Name` varchar(255) NOT NULL,
	`Code` varchar(30) NOT NULL,
	PRIMARY KEY (`ID`)
);

ALTER TABLE `Student` ADD CONSTRAINT `Student_fk0` FOREIGN KEY (`GroupID`) REFERENCES `StudentGroup`(`ID`);

ALTER TABLE `StudentGroup` ADD CONSTRAINT `StudentGroup_fk0` FOREIGN KEY (`TutorID`) REFERENCES `Teacher`(`ID`);

ALTER TABLE `StudentGroup` ADD CONSTRAINT `StudentGroup_fk1` FOREIGN KEY (`GroupLeaderID`) REFERENCES `Student`(`ID`);

ALTER TABLE `StudentGroup` ADD CONSTRAINT `StudentGroup_fk2` FOREIGN KEY (`DepartmentID`) REFERENCES `Department`(`ID`);

ALTER TABLE `StudentGroup` ADD CONSTRAINT `StudentGroup_fk3` FOREIGN KEY (`SpecialityID`) REFERENCES `Speciality`(`ID`);

ALTER TABLE `ExamHistory` ADD CONSTRAINT `ExamHistory_fk0` FOREIGN KEY (`StudentID`) REFERENCES `Student`(`ID`);

ALTER TABLE `ExamHistory` ADD CONSTRAINT `ExamHistory_fk1` FOREIGN KEY (`TestID`) REFERENCES `Exam`(`ID`);

ALTER TABLE `Department` ADD CONSTRAINT `Department_fk0` FOREIGN KEY (`UniversityID`) REFERENCES `University`(`ID`);

